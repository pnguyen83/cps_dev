public class SkeduloConstants {
    
    public static final string DATE_PARSE_FORMAT = 'M/d/yyyy';
    
    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final string JOB_STATUS_PEDNING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_STATUS_ON_SITE = 'On Site';
    
    public static final string ALLOCATION_STATUS_NO_ALLOCATION = 'No Allocation';
    public static final string ALLOCATION_STATUS_PARTIALLY_ALLOCATED = 'Partially Allocated';
    public static final string ALLOCATION_STATUS_FULLY_ALLOCATED = 'Fully Allocated';
    
    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCHED = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_ALLOCATION_STATUS_CHECK_IN = 'Checked In';
    public static final string JOB_ALLOCATION_STATUS_MODIFIED = 'Modified';

    public static final string RESOURCE_ROLE_REMOVALIST = 'Removalist';
    public static final string RESOURCE_ROLE_DIVER = 'Diver';
    public static final string RESOURCE_ROLE_PORTER_TEAM_LEADER = 'Porter team Leader';
    public static final string RESOURCE_ROLE_TECH_PORTER_ITFM = 'Tech Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_PORTER_ITFM = 'Server Porter - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TECH_ITFM = 'Server Tech - ITFM';
    public static final string RESOURCE_ROLE_DESK_TEAM_LEAD_ITFM = 'Desk Team Lead - ITFM';
    public static final string RESOURCE_ROLE_SERVER_TEAM_LEAD_ITFM = 'Server Team Lead - ITFM';
    
    public static final string RESOURCE_TYPE_PERSON = 'Person';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';
    
    public static final string GLOBAL_HOLIDAYS = 'global';
    
    public static final string USER_USERTYPE = '';
    public static final string BLANK = ' ';
    public static final string COMMA = ', ';

    public static final string JOB_ALLOCATION_STATUS_CONSOLE_PINK = 'pink';
    public static final string JOB_ALLOCATION_STATUS_CONSOLE_YELLOW = 'yellow';
    public static final string JOB_ALLOCATION_STATUS_CONSOLE_GREEN = 'green';
    public static final string JOB_TYPE_INTERNAL = 'Internal';
    public static final string JOB_TYPE_EXTERNAL = 'External';
    public static final string JOB_TYPE_BREAK = 'Break';

    public static final string NON_WORKING = 'Outside working hours';
    public static final string UNAVAILABLE = 'Unavailable';
    public static final string UNEDITABLE = 'Uneditable';
    public static final string TRAVEL_TIME_NOT_SUITABLE = 'Travel time greater than ';
    public static final string TRAVEL_TIME_NOT_ENOUGH = 'Not enough travel time';
    public static final string INVALID_GEOLOCATION = 'Geolocation of job is invalid';
    public static final string CONFLICT_BREAK_TIME = 'Conflict with break time of resource';
    public static final string BREAK_TIME = 'Break Time';
    public static final string IN_BLACK_LIST = 'In black list of this client';
}