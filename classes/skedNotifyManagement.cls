global class skedNotifyManagement {
	public static final String SKEDULO_END_POINT    = 'callout:Skedulo_API/notifications/';

	@future(callout = true)
    global static void sendNotification(Set<Id> jobAllocationIds) {
        notifyResources(jobAllocationIds);
    }

    global static void notifyResources(Set<Id> jobAllocationIds) {
        List<sked__Job_Allocation__c> jobAllocs = [
            SELECT Id, sked__Job__c, sked__Resource__c
            FROM sked__Job_Allocation__c
            WHERE Id IN :jobAllocationIds
            AND sked__Status__c NOT IN (:SkeduloConstants.JOB_ALLOCATION_STATUS_DELETED, :SkeduloConstants.JOB_ALLOCATION_STATUS_DECLINED)
        ];

        for (sked__Job_Allocation__c jobAlloc : jobAllocs) {
            ApiResponse result = notify(jobAlloc);

            List<NotificationResponseResult> results = (List<NotificationResponseResult>)result.data;
            for (NotificationResponseResult res : results) {
                if (res.error != null) {
                    jobAlloc.sked_API_Error__c = true;
                    jobAlloc.sked_API_Error_Message__c = res.error.message;
                }
            }
        }

        update jobAllocs;
    }

    private static ApiResponse notify(sked__Job_Allocation__c jobAlloc) {
        ApiResponse result = new ApiResponse();
        Http http      = new Http();
        HttpRequest req    = new HttpRequest();
        HttpResponse res  = new HttpResponse();
        NotificationRequest request = new NotificationRequest();
        request.jobId = jobAlloc.sked__Job__c;
        request.resourceId = jobAlloc.sked__Resource__c;

        //Set end point to Authenciate
        String endPoint = SKEDULO_END_POINT + 'notify';
        String jsonReq = JSON.serialize(request);

        System.debug('EndPoint = ' + EndPoint);
        req.setEndpoint( EndPoint );
        req.setMethod('POST');
        req.setTimeout(10000);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(jsonReq);
        string jsonResponse;

        try {
            if (test.isRunningTest()) {
                jsonResponse = '{"result":{"jobId":"a195E000001g061QAA","results":[{"resourceId":"a1H6E000000UrRzUAK","protocol":"push","error":{"errorType":"recipient_error","message":"No device found for resource a1H6E000000UrRzUAK","errorKey":"no_device"}}]}}';
            }
            else {
                res = http.send(req);
                jsonResponse = res.getBody();
            }
            System.debug(jsonResponse);
            NotificationResponseRoot respondRoot = (NotificationResponseRoot)JSON.deserialize(jsonResponse, NotificationResponseRoot.class);
            Map<Id, NotificationResponseResult> mpResults = new Map<Id, NotificationResponseResult>();
            if (respondRoot.result != null && respondRoot.result.results != null) {
                result.success = true;
                result.data = respondRoot.result.results;
            }
        }
        catch (JSONException jsonEx) {
            result.success = FALSE;
            result.errorMessage += ('Error: ' + jsonResponse);
        }
        
        return result;
    }

	global class ApiResponse {
		public boolean success;
		public object data;
        public string errorMessage;
	}

    global class NotificationRequest {
        public String jobId;
        public String resourceId;
    }

    global class NotificationResponseRoot {
        public NotificationResponse result;
    }

    global class NotificationResponse {
        public String jobId;
        public List<NotificationResponseResult> results;
    }

    global class NotificationResponseResult {
        public String resourceId;
        public String protocol;
        public NotificationError error;
    }

    global class NotificationError {
        public String errorType;
        public String message;
    }
}