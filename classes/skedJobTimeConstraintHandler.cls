public class skedJobTimeConstraintHandler {
	@InvocableMethod(label='Create Job Time Constraint' description='Create a process builder that fires when a Job is created with a type of "Break"')
  	public static void createJobTimeConstraint(List<Id> lstJobIds)
    {
    	System.debug('lstJobIds = ' + lstJobIds);
    	List<sked__Job_Time_Constraint__c> lstJTCs = new List<sked__Job_Time_Constraint__c>();
	    for (sked__Job__c job : [SELECT id, sked__Start__c, sked__Finish__c, sked__Timezone__c 
	    							FROM sked__Job__c 
	    							WHERE Id IN :lstJobIds]) {
                                        if (job.sked__Start__c != null && job.sked__Timezone__c != null) {
                                            DateTime startTime = skedUtils.GetStartOfDate(job.sked__Start__c, job.sked__Timezone__c);
                                            DateTime jtcStart = startTime.addMinutes(660);
                                            DateTime jcTEnd =  startTime.addMinutes(870);
                                            sked__Job_Time_Constraint__c jtc = new sked__Job_Time_Constraint__c(
                                                sked__Start_After__c = jtcStart,
                                                sked__Start_Before__c = jcTEnd,
                                                sked__Required__c = true,
                                                sked__Job__c = job.id
                                            );
                                            lstJTCs.add(jtc);
                                        }
	    	
	    }
	    System.debug('lstJTCs = ' + lstJTCs);
	    if (!lstJTCs.isEmpty()) {
	    	insert lstJTCs;
	    }
	}
}