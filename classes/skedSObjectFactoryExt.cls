public class skedSObjectFactoryExt extends skedSObjectFactory{
	
    /**
    * @description override the same method of the parent class to initialize more objects
    * @param obj
    */
    public override void initMore(){
        
        //Register a new Opportunity
		this.newObject(Opportunity.sObjectType).fields(new Map<String, Object>{
				'Name'						=> 'Test Opportunity',
				'AccountId' 				=> this.newRelationship(Account.sObjectType)
			}).creationOrder(25);
		//Register a new Job Type
		this.newObject(Job_Type__c.sObjectType).fields(new Map<String, Object>{
				'Name'						=> 'Test Job Type'
			});

		//Register a new Job Type
		this.newObject(Job_Type_Task__c.sObjectType).fields(new Map<String, Object>{
				'Job_Type__c'				=> this.newRelationship(Job_Type__c.sObjectType)
			});

		//Register a new Resource Requirement Defaults
		this.newObject(sked_Resource_Requirement_Defaults__c.sObjectType).fields(new Map<String, Object>{
				'Name'						=> 'Test Job Type',
				'sked_Job_Type__c'			=> this.newRelationship(Job_Type__c.sObjectType),
				'sked_Category__c'			=> skedSObjectFactory.getPicklistValue(sked_Resource_Requirement_Defaults__c.sked_Category__c).get(0)
			});

		//Link jobs to the Opportunity
		this.getObject(sked__Job__c.sObjectType).fields(new Map<String, Object>{
				'Opportunity__c'			=> this.newRelationship(Opportunity.sObjectType)//,
                //'sked__Start__c'			=> skedSObjectFactory.getDTList( DateTime.newInstance(System.now().date(), Time.newInstance(9, 0,0,0)).addDays(-3), 6 )
			});

		

		this.newObject(sked__Resource_Requirement_Tag__c.sObjectType).fields(new Map<String, Object>{
				'sked__Resource_Requirement__c' 	=> this.newRelationship(sked__Resource_Requirement__c.sObjectType),
				'sked__Tag__c' 						=> this.newRelationship(sked__Tag__c.sObjectType)
			});

		this.getObject(sked__Resource__c.sObjectType).fields(new Map<String, Object>{
				'sked__Resource_Type__c' 						=> 'Person',
				'sked__GeoLocation__Latitude__s' 						=> -27.47562,
				'sked__GeoLocation__Longitude__s'						=> 153.028056
			});

/*		this.newObject(sked__Resource_Requirement__c.sObjectType).fields(new Map<String, Object>{
				'sked_Category__c'	=>	getPicklistValue(sked__Resource__c.sked__Category__c).get(0),
				'sked__Quantity__c'	=>	1,
				'sked__Job__c' 	=>	this.newRelationship(sked__Job__c.sObjectType),
				'sked_Category_Type__c'	=>	'Person'
			});*/
		//this.getObject(sked__Job_Allocation__c.sObjectType).field('sked__Resource_Requirement__c', this.newRelationship(sked__Resource_Requirement__c.sObjectType).size(1));
        
    }
}