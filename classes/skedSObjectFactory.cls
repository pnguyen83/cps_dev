/**
* @description base class SObjectModel
*/
public virtual class skedSObjectFactory{
	
	//=============================================================================================
	// CONSTANTS
	//=============================================================================================
	public final String US 			= 'US';
	public final String AU 			= 'AU';
	public final Map<String,String> MAP_DEFAULT_ADDR = new Map<String,String>{
		AU 	=> '79 Mclachlan Fortitude Valley st Brisbane QLD',
		US 	=> '228 Park Ave S, New York'
	};

	public final Map<String, List<Object>> MAP_REGIONS{
    	get{
    		if(MAP_REGIONS == null){
    			MAP_REGIONS = new Map<String, List<Object>>{
			    	US => new List<Object>{'US/Central' ,'US/Mountain', 'US/Michigan'},
			    	AU => new List<Object>{'Australia/Queensland','Australia/NSW','Australia/Victoria', 'Australia/Sydney'}
			    };
    		}
    		return MAP_REGIONS;
    	}
    	set;
    }

    public final list<String> RESOURCE_TYPES = new List<String>{ 'Person', 'Asset' };

    //=============================================================================================
	// VARIABLES
	//=============================================================================================
	// SObjects (in order of dependency) to be generated
	public list<SObjectModel> sObjectList;
	private Map<Schema.sObjectType, list<sObject>> mapSObjectType2ObjectList;

	public skedSObjectFactory(){
		sObjectList 				= new list<SObjectModel>();
		mapSObjectType2ObjectList 	= new Map<Schema.sObjectType, list<sObject>>();
	}

	/**
    * @description create all registered objects
    * 
    */
	public Map<Schema.sObjectType, list<sObject>> create(){
		sObjectList.sort();
		
		for(SObjectModel obj : sObjectList){
			//Fill relationships
			fillRelationships(obj);
			//Generate data
			list<sObject> records = obj.create(true);
			mapSObjectType2ObjectList.put( obj.sObjectType, records );
		}
		return mapSObjectType2ObjectList;
	}

	/**
    * @description Initialize a sample set of test data 
    *
    */
	public skedSObjectFactory init(){
		//User
		Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
       
    	this.newObject(User.sObjectType).fields(new Map<String, Object>{
				'Alias' 					=> 'admin',
				'Email'						=> 'test@email.com',
				'EmailEncodingKey' 			=> 'UTF-8',
				'LastName'					=>'Admin', 
				'LanguageLocaleKey'			=>'en_US', 
	            'LocaleSidKey'				=>'en_US', 
	            'ProfileId' 				=> p.Id, 
	            'TimeZoneSidKey'			=>UserInfo.getTimeZone().getID(), 
	            'UserName'					=>'testuser93274328423@test.com'
			});
		//Account
		this.newObject(Account.sObjectType).field('Name', 'Test Account');

		//Region
		this.newObject(sked__Region__c.sObjectType).fields(new Map<String, Object>{
				'sked__Country_Code__c' 	=> AU,
				'Name'						=> UserInfo.getTimeZone().getID(),
				'sked__Timezone__c'			=> UserInfo.getTimeZone().getID()
			});
		//Location
		this.newObject(sked__Location__c.sObjectType).fields(new Map<String, Object>{
				'sked__GeoLocation__Latitude__s' 	=> 39.298205,
				'sked__GeoLocation__Longitude__s' 	=> -76.613203,
				'sked__Address__c' 					=> 'Skedulo Office',
				//Lookup
				'sked__Account__c'					=> this.newRelationship(Account.sObjectType).size(1),
				'sked__Region__c'					=> this.newRelationship(sked__Region__c.sObjectType).size(1)
			});

		//Contact
		this.newObject(Contact.sObjectType).fields(new Map<String,Object>{
				'FirstName' 				=> 'Test Contact',
				'MobilePhone' 				=> '(090)3699780',
				'Email' 						=> 'test@abc.com',
				//Lookup
				'AccountId' 				=> this.newRelationship(Account.sObjectType),
				'sked__Region__c' 			=> this.newRelationship(sked__Region__c.sObjectType).size(1)
			});

		//Job
		this.newObject(sked__Job__c.sObjectType).fields(new Map<String, Object>{
				'sked__Duration__c' 		=> 60,
				'sked__Start__c' 			=> DateTime.newInstance(System.now().date(), Time.newInstance(9, 0,0,0)),
				'sked__Job_Status__c' 		=> new List<Object> { 'Pending Dispatch' },
				'sked__Address__c'			=> MAP_DEFAULT_ADDR.get(AU),
				'sked__Type__c'				=> getPicklistValue(sked__Job__c.sked__Type__c).get(0),
				//Lookup
				'sked__Contact__c'			=> this.newRelationship(Contact.sObjectType),
				'sked__Region__c' 			=> this.newRelationship(sked__Region__c.sObjectType)
			});

		//Resource
		this.newObject(sked__Resource__c.sObjectType).fields(new Map<String, Object>{
				'Name' 						=> 'Test Resource',
				'sked__Resource_Type__c' 	=> 'Person',
				'sked__Home_Address__c'		=> MAP_DEFAULT_ADDR.get(AU),
				'sked__Category__c' 		=> getPicklistValue(sked__Resource__c.sked__Category__c).get(0),
				//'sked__User__c'				=> UserInfo.getUserId(),
				'sked__Is_Active__c'      	=> true,
                'sked__Weekly_Hours__c'   	=> 40,
                //Lookup
                'sked__Primary_Region__c' 	=> this.newRelationship(sked__Region__c.sObjectType)
			});

		//Tag
		this.newObject(sked__Tag__c.sObjectType).fields(new Map<String, Object>{
				'Name' 						=> 'Communication'
			});

		//Resource Tag
		this.newObject(sked__Resource_Tag__c.sObjectType).fields(new Map<String, Object>{
				'sked__Resource__c' 		=> this.newRelationship(sked__Resource__c.sObjectType),
				'sked__Tag__c' 				=> this.newRelationship(sked__Tag__c.sObjectType)
			});

		this.newObject(sked__Resource_Requirement__c.sObjectType).fields(new Map<String, Object>{
				'sked__Quantity__c'			=> 1,
				'sked__Job__c' 				=> this.newRelationship(sked__Job__c.sObjectType),
				'sked_Category__c'			=> skedSObjectFactory.getPicklistValue(sked__Resource_Requirement__c.sked_Category__c).get(0)
			});

		//Job Allocation
		this.newObject(sked__Job_Allocation__c.sObjectType).fields(new Map<String, Object>{
				'sked__Resource__c' 		=> this.newRelationship(sked__Resource__c.sObjectType),
				'sked__Job__c' 				=> this.newRelationship(sked__Job__c.sObjectType),
				'sked__Resource_Requirement__c'	=> this.newRelationship(sked__Resource_Requirement__c.sObjectType)
			});

		//Availability Template
		this.newObject(sked__Availability_Template__c.sObjectType);

		//Availability Template Entry
		this.newObject(sked__Availability_Template_Entry__c.sObjectType).fields(new Map<String, Object>{
				'sked__Start_Time__c' 		=> 800,
				'sked__Finish_Time__c' 		=> 2300,
				'sked__Is_Available__c'		=> true,
				'sked__Weekday__c'			=> getPicklistValue(sked__Availability_Template_Entry__c.sked__Weekday__c),
				//Lookup
				'sked__Availability_Template__c'	=> this.newRelationship(sked__Availability_Template__c.sObjectType)
			});

		//Availability Template Resource
		this.newObject(sked__Availability_Template_Resource__c.sObjectType).fields(new Map<String, Object>{
				'sked__Availability_Template__c' 	=> this.newRelationship(sked__Availability_Template__c.sObjectType),
				'sked__Resource__c' 				=> this.newRelationship(sked__Resource__c.sObjectType)
			});

		//Availability
		this.newObject(sked__Availability__c.sObjectType).fields(new Map<String, Object>{
				'sked__Is_Available__c' 	=> new list<Object>{FALSE, TRUE},
				'sked__Status__c'			=> 'Approved',
				'sked__Start__c' 			=> DateTime.newInstance(System.now().date(), Time.newInstance(0,0,0,0)),
				'sked__Finish__c' 			=> DateTime.newInstance(System.now().addDays(1).date(), Time.newInstance(0,0,0,0)),
				'sked__Timezone__c'			=> UserInfo.getTimeZone().getID(),
				//Lookup
				'sked__Resource__c' 		=> this.newRelationship(sked__Resource__c.sObjectType)
			});

		//Activity
		this.newObject(sked__Activity__c.sObjectType).fields(new Map<String, Object>{
				'sked__Start__c' 			=> DateTime.newInstance(System.now().date(), Time.newInstance(0,0,0,0)),
				'sked__End__c' 				=> DateTime.newInstance(System.now().addDays(1).date(), Time.newInstance(0,0,0,0)),
				'sked__Type__c'				=> getPicklistValue(sked__Activity__c.sked__Type__c).get(0),
				'sked__Address__c'    		=> '28 Tuckett Rd, Salisbury, Queensland, AUS',
                'sked__Notes__c'      		=> 'Have a nice lunch',
                'sked__Timezone__c'   		=> UserInfo.getTimeZone().getID(),
                'sked__GeoLocation__Latitude__s' 	=> 39.298205,
                'sked__GeoLocation__Longitude__s' 	=> -76.613203,
                //Lookup
                'sked__Resource__c' 		=> this.newRelationship(sked__Resource__c.sObjectType).filter('sked__Resource_Type__c','Person')
			});

		//Holiday
		this.newObject(sked__Holiday__c.sObjectType).fields(new Map<String, Object>{
				'Name' 						=> 'Global Holidays',
	            'sked__Start_Date__c' 		=> System.today().addDays(3),
	            'sked__End_Date__c' 		=> System.today().addDays(3),
	            'sked__Global__c' 			=> new List<Object>{true, false}
			});

		//Holiday Region
		this.newObject(sked__Holiday_Region__c.sObjectType).fields(new Map<String, Object>{
				'sked__Region__c' 			=> this.newRelationship(sked__Region__c.sObjectType).size(1),
            	'sked__Holiday__c' 			=> this.newRelationship(sked__Holiday__c.sObjectType).filter('sked__Global__c', false)
			});

		initMore();

		return this;
	}

	/**
    * @description 
    * @param obj
    */
	public void fillRelationships(SObjectModel obj){
		for(String fieldName : obj.mapFieldname2Values.keySet()){
			Object value = obj.mapFieldname2Values.get(fieldName);
			if(value instanceof List<Object>){
				 Object firstValue = new List<Object>( (List<Object>)value ).get(0);
				 if(firstValue instanceof RelationshipModel){
				 	RelationshipModel relationship = (RelationshipModel)firstValue;
				 	if(!mapSObjectType2ObjectList.containsKey( relationship.sObjectType )) continue;
				 	List<Object> recordIDs = new List<Object>();
				 	Integer count = 0;
					for(sObject rec : mapSObjectType2ObjectList.get( relationship.sObjectType )){
						//Apply filter
						boolean matchFilter = true;
						for(String filteredFieldName : relationship.mapFilter.keySet()){
							if(rec.get(filteredFieldName) != relationship.mapFilter.get(filteredFieldName)){
								matchFilter = false;
								break;
							}
						}
						if(!matchFilter) continue;
						recordIDs.add( String.valueOf(rec.get('Id')) );
						count++;
						if(count >= relationship.noOfRecords) break;//Limit the number of records
					}
					obj.field(fieldName, recordIDs);
				 }
			}
		}
	}

	/**
	* @description 
	* @param sObjectType
	*/
	public skedSObjectFactory removeObject(Schema.sObjectType sObjectType){
		for(Integer j = 0; j < sObjectList.size(); j++){
		   if(sObjectList.get(j).sObjectType == sObjectType){
		        sObjectList.remove(j);
		        break;
		   }
		}
		return this;
	}

	/**
	* @description
	* @param sObjectType
	*/
	public skedSObjectFactory removeObjects(List<Schema.sObjectType> sObjectTypes){
		for(Schema.sObjectType objType : sObjectTypes){
		   removeObject(objType);
		}
		return this;
	}

	/**
	* @description 
	* @param sObjectType
	*/
	public SObjectModel getObject(Schema.sObjectType sObjectType){
		for(SObjectModel obj : sObjectList){
			if(obj.sObjectType == sObjectType) return obj;
		}
		return null;//this.newObject(sObjectType);
	}

	/**
	* @description
	* @param sObjectType
	*/
	public List<sObject> getSObjects(Schema.sObjectType sObjectType){
		return mapSObjectType2ObjectList.get(sObjectType);
	}

	/**
	* @description 
	* @param sObjectType 
	*/
	public SObjectModel newObject(Schema.sObjectType sObjectType){
		Integer greatestCreationOrder = 10;
		if(!sObjectList.isEmpty()){
			sObjectList.sort();
			greatestCreationOrder += sObjectList.get( sObjectList.size()-1 ).creationOrder;
		}
		sObjectModel newObject = initNewObject(sObjectType).creationOrder(greatestCreationOrder);
		sObjectList.add(newObject);
		return newObject;
	}

	/**
	* @description 
	* @param sObjectType
	* @param creationOrder
	*/
	public RelationshipModel newRelationship(Schema.sObjectType sObjectType){
		return new RelationshipModel(sObjectType);
	}

	//=============================================================================================
	// VIRTUAL METHODS
	//=============================================================================================
	/**
    * @description This method will be overridden by a subclass to initialize more objects
    * 
    */
    public virtual void initMore(){

    }

    /**
    * @description This method will be overridden by a subclass to initialize an instance of a class that extends SObjectModel
    * 
    */
    public virtual SObjectModel initNewObject(Schema.sObjectType sObjectType){
    	return new SObjectModel(sObjectType);
    }
	
	//=============================================================================================
	// INNER CLASSES
	//=============================================================================================
	/**
    * @description base class SObjectModel
    */
	public virtual class SObjectModel implements Comparable{

		public Schema.sObjectType sObjectType;
		public Map<String, List<Object>> mapFieldname2Values;
		public Map<String, Object> mapFilter;
		public Integer multiplyBy;
		public Integer creationOrder;

		public SObjectModel(Schema.sObjectType ot){
			mapFieldname2Values 	= new Map<String, List<Object>>();
			this.sObjectType 		= ot;
			this.multiplyBy 		= 1;
			this.mapFilter 			= new Map<String, Object>();
		}

		/**
		* @description
		* @param doInsert
		*/
		public List<sObject> create(boolean doInsert){
			String sObjectName = sObjectType.getDescribe().getName();
			list<sObject> result = new List<sObject>();
			Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName); 
			if(mapFieldname2Values == null || mapFieldname2Values.keySet().isEmpty()){
				result.addAll( skedTestDataFactoryBase.createSObjectList( sObjectName, false, multiplyBy));
			}else{
				List<Map<String, Object>> listMapFieldname2Values = combine( mapFieldname2Values );
				for(Map<String, Object> mapFieldname2Value : listMapFieldname2Values){
					result.addAll( skedTestDataFactoryBase.createSObjectList( sObjectName, mapFieldname2Value, false, multiplyBy) );
				}
			}
			//format data before insert
			format(sObjectType, result);
			system.debug('sObjectName: ' + sObjectName);
			system.debug('sObjectName: ' + JSON.serialize(result));
			if(doInsert) insert result;
			return result;
		}

		/**
		* @description Format data to meed per-defined rules
		* @param sObjectType
		* @param objects
		*/
		public void format(Schema.sObjectType sObjectType, list<sObject> objects){
			if(sObjectType == sked__Job__c.sObjectType){
				for(sObject obj : objects){
					if(obj.get('sked__Start__c') != null && obj.get('sked__Duration__c')!=null){
						obj.put( 'sked__Finish__c', ((DateTime)obj.get('sked__Start__c')).addMinutes( Integer.valueOf(obj.get('sked__Duration__c')) ) );
					}
				}
			}
			formatMore(sObjectType, objects);
		}

		/**
		* @description This method will be overridden by a subclass to format data before creation
		* @param sObjectType
		* @param obj
		*/
		public virtual void formatMore(Schema.sObjectType sObjectType, List<sObject> objects){

		}

		/**
		* @description
		* @param mapFieldname2Values
		*/
		public SObjectModel fields(Map<String, object> mapFieldname2Values){
			for(String fieldName : mapFieldname2Values.keySet()){
				Object value = mapFieldname2Values.get(fieldName);
				if(value instanceof List<Object>) this.mapFieldname2Values.put( fieldName, (List<Object>)value );
				else if(value instanceOf RelationshipModel){
					this.mapFieldname2Values.put( fieldName, new List<Object>{ value });
				}
				else this.mapFieldname2Values.put( fieldName, new List<Object>{ value });
			}
			
			return this;
		}

		/**
		* @description
		* @param fieldName
		* @param values
		*/
		public SObjectModel field(String fieldName, Object value){
			this.fields(new Map<String, Object>{ fieldName => value });
			return this;
		}

		/**
		* @description
		* @param multiplyBy
		*/
		public SObjectModel x(Integer multiplyBy){
			this.multiplyBy 	= multiplyBy;
			return this;
		}

		/**
		* @description
		* @param creationOrder
		*/
		public SObjectModel creationOrder(Integer creationOrder){
			this.creationOrder = creationOrder;
			return this;
		}

		/**
		* @description
		* @param compareTo
		*/
		public Integer compareTo(Object compareTo) {
			SObjectModel obj = (SObjectModel)compareTo;
			return (this.creationOrder > obj.creationOrder ? 1 :-1);
		} 
	}

	/**
    * @description base class SObjectModel
    */
	public class RelationshipModel{
		public Schema.sObjectType sObjectType;
		public Integer noOfRecords;
		public Map<String,Object> mapFilter;

		public RelationshipModel(Schema.sObjectType ot){
			this.sObjectType 		= ot;
			this.noOfRecords 		= 100;
			this.mapFilter 			= new Map<String,Object>();
		}

		/**
		* @description
		* @param filter
		*/
		public RelationshipModel filter(String fieldName, Object value){
			this.mapFilter.put( fieldName, value );
			return this;
		}

		/**
		* @description
		* @param size
		*/
		public RelationshipModel size(Integer noOfRecords){
			this.noOfRecords 		= noOfRecords;
			return this;
		}
	}

	//=============================================================================================
	// UTILITY METHODS
	//=============================================================================================
    /**
    * @description: get a list of active values of a picklist field
    * @param: a picklist field which is a sObjectField type, for example Opportunity.StageName, sked__Job__c.sked__Job_Status__c
    * @return: a list of active values of the picklist field
    */
    public static list<String> getPicklistValue(sObjectField field){
    	Schema.DescribeFieldResult fieldResult = field.getDescribe();
    	list<String> result = new list<String>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

       for( Schema.PicklistEntry f : ple){
          if(f.isActive()) result.add( f.getValue() );
       }
       return result;
    }
    
    /**
	* @description: transform a Map<K,Set<V>> into the List<Map<K,V>> getting all possible combinations
	*
	*/
	public static List<Map<String, Object>> combine(Map<String, List<Object>> originalMap) {
		List<Map<String, Object>> finalList = new List<Map<String, Object>>();
		Map<String, Object> current = new Map<String, Object>();
		Integer index = 0;

	    combine(index, current, originalMap, finalList);

	    return finalList;
	}

    /**
	 * @description transform a Map<K,Set<V>> into the List<Map<K,V>> getting all possible combinations
	 * @param <K> The type of the key
	 * @param <V> The type of the value
	 * @param index The index of the current key to inspect
	 * @param current The current map being built by recursion
	 * @param map The original input
	 * @param list The result
	 */ 
	public static void combine(Integer index, Map<String, Object> current, Map<String, List<Object>> originalMap, List<Map<String, Object>> finalList) {

	    if(index == originalMap.size()) { // if we have gone through all keys in the map
	        Map<String, Object> newMap = new Map<String, Object>();
	        
	        for(String key: current.keySet()) {          // copy contents to new map.    
	            newMap.put(key, current.get((String)key));               
	        }           
	        finalList.add(newMap); // add to result.
	    } else {
	        String currentKey = new list<String>(originalMap.keySet()).get(index); // take the current key
	        for(Object value: originalMap.get(currentKey)) {
	            current.put((String)currentKey, value); // put each value into the temporary map
	            Combine(index + 1, current, originalMap, finalList); // recursive call
	            current.remove(currentKey); // discard and try a new value
	        }
	    }
	}

	/**
	* @description: generate a list of continuous DateTime
	* @param start 
	* @param noOfDays 
	*/
	public static List<Object> getDTList(DateTime start, Integer noOfDays) {
		list<Object> result = new List<Object>();
		DateTime temp;
		for(Integer i = 0; i<= noOfDays; i++){
			temp = start.addDays(i);
			result.add(temp);
		}
		return result;
	}

	/**
    * @description get object's record type developer names
    * @param objectAPIName
    * @param developerNames: All - get all developer names
    *                   Otherwise: a list of developer names, seperated by commas
    */
    /*
    public static String getRecordTypeIDByDeveloperName(String objectAPIName, String developerName){
        if(String.isBlank(developerName)) return '';
        //Get IDs by developer names
        for(RecordType rt : [select Id, Name, DeveloperName from RecordType where sobjecttype=:objectAPIName]){
            if(rt.DeveloperName == developerName) return rt.Id;
        }
        return null;
    }
    */

    /**
    * @description gerate a random 10-character string
    */
    /*
    public static String generateRandomString() {
	    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
	    String randStr = '';
	    while (randStr.length() < 10) {
	       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
	       randStr += chars.substring(idx, idx+1);
	    }
	    return randStr; 
	}
	*/
}