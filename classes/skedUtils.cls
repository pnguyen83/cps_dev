public class skedUtils {

    public static string TIMESLOT_KEY_FORMAT = 'dd/MM/yyyy hh:mm a';
    public static string DATE_FORMAT = 'yyyy-MM-dd';
    public static string DATE_ISO_FORMAT = '"yyyy-MM-dd"';

    public static Date getDate(DateTime input, string timezoneSidId) {
        string dateIsoString = input.format(DATE_FORMAT, timezoneSidId);
        return (Date)Json.deserialize('"' + dateIsoString + '"', Date.class);
    }

    public static DateTime GetStartOfDate(string dateIsoString, string timezoneSidId) {
        Date tempDate = (Date)Json.deserialize(dateIsoString, Date.class);
        DateTime result = DateTime.newInstance(tempDate, time.newInstance(0, 0, 0, 0));
        result = skedUtils.ConvertBetweenTimezones(result, timezoneSidId, UserInfo.getTimeZone().getID());
        return result;
    }

    public static DateTime GetStartOfDate(DateTime input, string timezoneSidId) {
        String dateIsoString = input.format(DATE_ISO_FORMAT, timezoneSidId);
        return GetStartOfDate(dateIsoString, timezoneSidId);
    }

    public static DateTime GetEndOfDate(DateTime input, string timezoneSidId) {
        return GetStartOfDate(input, timezoneSidId).addDays(1);
    }
   /*
    public static string ConvertDateToIsoString(Date input) {
        string result = Json.serialize(input).replace('"', '');
        return result;
    } */

    /*public static Datetime convertToTimezone(Datetime currentDT, string TZ) {
        Timezone SetTZ = Timezone.getTimeZone(TZ); // get target timezone
        Timezone currentTZ = Userinfo.getTimeZone(); // get current user timezone

        integer setOffSet = SetTZ.getoffset(currentDT); //calculate target timezone offset
        integer curOffSet = currentTZ.getoffset(currentDT); //calculate current timezone offset

        integer resOffSet = curOffSet - setOffSet;

        integer offSet = resOffSet / (1000);
        DateTime localDT = currentDT.addSeconds(offSet);
        return localDT;
    }*/
    /*
    public static Date ConvertToDateValue(string dateString) {
        String[] temp = dateString.split('-');
        return Date.newInstance(Integer.valueOf(temp[0]), Integer.valueOf(temp[1]), Integer.valueOf(temp[2]));
    } */

    public static DateTime ConvertBetweenTimezones(DateTime input, string fromTimezoneSidId, string toTimezoneSidId) {
        if (fromTimezoneSidId == toTimezoneSidId) {
            return input;
        }
        TimeZone fromTz = Timezone.getTimeZone(fromTimezoneSidId);
        Timezone toTz = Timezone.getTimeZone(toTimezoneSidId);
        integer offsetMinutes = toTz.getOffset(input) - fromTz.getOffset(input);
        offsetMinutes = offsetMinutes / 60000;
        input = input.addMinutes(offsetMinutes);
        return input;
    }

    // @param inputTime: 730
    // @result: 730 will be considered as 7 hour and 30 minutes so the result should be 7 * 60 + 30 = 450
    public static integer ConvertTimeNumberToMinutes(integer inputTime) {
        return integer.valueOf(inputTime / 100) * 60 + Math.mod(inputTime, 100);
    }

    /*
    public static Set<Date> getHolidaysByRegion(Id regionId) {
        Set<Date> allHolidays = new Set<Date>();

        Map<string, Set<Date>> mapHolidays = skedUtils.getHolidays(system.today());
        if (mapHolidays.containsKey(SkeduloConstants.GLOBAL_HOLIDAYS)) {
            Set<Date> globalHolidays = mapHolidays.get(SkeduloConstants.GLOBAL_HOLIDAYS);
            allHolidays.addAll(globalHolidays);
        }
        if (mapHolidays.containsKey(regionId)) {
            Set<Date> regionHolidays = mapHolidays.get(regionId);
            allHolidays.addAll(regionHolidays);
        }

        return allHolidays;
    }

    public static Set<Date> getHolidaysByRegion(Id regionId,Date StartDate) {
        Set<Date> allHolidays = new Set<Date>();

        Map<string, Set<Date>> mapHolidays = getHolidays(StartDate);
        if (mapHolidays.containsKey('global')) {
            Set<Date> globalHolidays = mapHolidays.get('global');
            allHolidays.addAll(globalHolidays);
        }
        if (mapHolidays.containsKey(regionId)) {
            Set<Date> regionHolidays = mapHolidays.get(regionId);
            allHolidays.addAll(regionHolidays);
        }

        return allHolidays;
    }*/

    public static Map<string, Set<Date>> getHolidays(Date currentDate) {
        Map<string, Set<Date>> mapHolidays = new Map<string, Set<Date>>();
        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE
                                                     AND sked__End_Date__c >= :currentDate];
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c,
                                                            sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c
                                                            WHERE sked__Holiday__r.sked__End_Date__c >= :currentDate];

        Set<Date> globalHolidays = new Set<Date>();
        for (sked__Holiday__c globalHoliday : skedGlobalHolidays) {
            Date tempDate = globalHoliday.sked__Start_Date__c;
            while (tempDate <= globalHoliday.sked__End_Date__c) {
                globalHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
        }
        mapHolidays.put('global', globalHolidays);

        for (sked__Holiday_Region__c regionHoliday : skedRegionHolidays) {
            Set<Date> regionHolidays;
            if (mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                regionHolidays = mapHolidays.get(regionHoliday.sked__Region__r.Name);
            } else {
                regionHolidays = new Set<Date>();
            }

            Date tempDate = regionHoliday.sked__Holiday__r.sked__Start_Date__c;
            while (tempDate <= regionHoliday.sked__Holiday__r.sked__End_Date__c) {
                regionHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }

            if (!mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                mapHolidays.put(regionHoliday.sked__Region__r.Name, regionHolidays);
            }
        }
        return mapHolidays;
    }

    public static List<skedModels.selectOption> getPickListValues(string objectApiName, string fieldApiName) {
        List<skedModels.selectOption> results = new List<skedModels.selectOption>();

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        SObjectField fieldType = mapFields.get(fieldApiName);
        DescribeFieldResult fieldResult = fieldType.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple) {
            skedModels.selectOption option = new skedModels.selectOption(f.getValue(), f.getLabel());
            results.add(option);
        }
        return results;
    }
    /*
    public static integer daySequenceOfWeek(string weekday) {
        string lowerCaseWeekDay = weekday.toLowerCase();
        return lowerCaseWeekDay == 'mon' ? 1 :
                lowerCaseWeekDay == 'tue' ? 2 :
                lowerCaseWeekDay == 'wed' ? 3 :
                lowerCaseWeekDay == 'thu' ? 4 :
                lowerCaseWeekDay == 'fri' ? 5 :
        		lowerCaseWeekDay == 'sat' ? 6 : 7;
    } */
}