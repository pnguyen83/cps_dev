public class skedLightningAllocateModalDataCtrl {
	@AuraEnabled
    public static String getOppId(String jobId) {
        List<sked__Job__c> jobs = [
            select Id, Opportunity__c
            from sked__Job__c
            where Id = :jobId
        ];

        if (!jobs.isEmpty()) {
            return jobs[0].Opportunity__c;
        }

        return null;
    }
}