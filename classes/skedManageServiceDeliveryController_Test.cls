@isTest
private class skedManageServiceDeliveryController_Test {
	@isTest
	private static void testController(){
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		Test.startTest();
		//Get an Opportunity from the data map
		Opportunity opp = (Opportunity)objFactory.getSObjects(Opportunity.sObjectType).get(0);
		
		//Positive tests
		skedManageServiceDeliveryController.getOpportunity(opp.Id);
		skedManageServiceDeliveryController.getJobDetails(opp.Id);
		//Negative tests
		skedManageServiceDeliveryController.getOpportunity(null);
		skedManageServiceDeliveryController.getJobDetails(null);

		skedManageServiceDeliveryController.getConfigData(opp.Id);
		skedManageServiceDeliveryController.getAssociatedContacts(opp.Id, 'Test');

		//Get a Job from the data map
		sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);

		skedManageServiceDeliveryController.getJobDetailsById(opp.Id, job.Id);
		skedManageServiceDeliveryController.replicateAllocation(job.Id);

		//Get a Job Allocation from the data map
		sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
		skedManageServiceDeliveryController.allocation allocation = new skedManageServiceDeliveryController.allocation();
		allocation.jaId = ja.Id;
		allocation.jobId = ja.sked__Job__c;
		allocation.resId = ja.sked__Resource__c;
		skedManageServiceDeliveryController.notifyResource(allocation);

		sked__Region__c region 	= (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
		String startDateString 	= System.now().format('yyyy-MM-dd');

		skedManageServiceDeliveryController.getAvailResources(job.Id, region.Id, startDateString, 1000, 60, 10000);

		//Test data model initialization
		sked__Location__c location = (sked__Location__c)objFactory.getSObjects(sked__Location__c.sObjectType).get(0);
		skedManageServiceDeliveryController.locationOptionModel loModel = new skedManageServiceDeliveryController.locationOptionModel(location);

		List<sked_Resource_Requirement_Defaults__c> rrdList = (list<sked_Resource_Requirement_Defaults__c>)objFactory.getSObjects(sked_Resource_Requirement_Defaults__c.sObjectType);
		skedManageServiceDeliveryController.resourceDefault rd = new skedManageServiceDeliveryController.resourceDefault('Test job Type', rrdList);

		Job_Type_Task__c task = (Job_Type_Task__c)objFactory.getSObjects(Job_Type_Task__c.sObjectType).get(0);
		skedManageServiceDeliveryController.JobTypeTask jtTask = new skedManageServiceDeliveryController.JobTypeTask(task);

		Test.stopTest();
	}

	static testmethod void getDashboardDurationTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
		Contact contact = (Contact)objFactory.getSObjects(Contact.sObjectType).get(0);
		Account account = (Account)objFactory.getSObjects(Account.sObjectType).get(0);
		sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
		//sked__Availability__c avail = [Select id from sked__Availability__c where sked__Resource__c=:resource.Id AND sked__Is_Available__c = true];
		/*
		objFactory.newObject(sked__Availability__c.sObjectType).fields(new Map<String, Object>{
				'sked__Resource__c'				=> resource.id,
				'sked__Is_Available__c'			=> true,
				'sked__Start__c'				=> DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0)),
				'sked__Finish__c'				=> DateTime.newInstance(Date.today(), Time.newInstance(24, 0, 0, 0)),
				'sked__Status__c'				=> 'Approved'
			}).creationOrder(26);
		*/
		sked__Region__c region 	= (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
		String startDateString 	= System.now().format('yyyy-MM-dd');
		skedModels.requiredResource rr = new skedModels.requiredResource();
		rr.category 	= resource.sked__Category__c;
		rr.numOfResources = 1;
		rr.IsChanged = true;

		skedModels.ResourceFilter resourceFilter = new skedModels.ResourceFilter();
		resourceFilter.requiredResources.add(rr );
		resourceFilter.contactId = contact.Id;
		resourceFilter.accountId = account.Id;
		resourceFilter.maxTravelTime 	= 1000;

		test.startTest();
		skedRemoteResultModel result = skedManageServiceDeliveryController.getDashboardDuration(resourceFilter, startDateString, region.Id, 60, 100, 0.1, 0.1, 60);
		System.debug('result = ' + result);
		test.stopTest();
	}

	static testmethod void saveJobTestUpdateJob() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
		String startDateString 	= System.now().format('yyyy-MM-dd');
		test.startTest();
		Id jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Timezone__c, sked__Finish__c, sked_Job_Completion_Status__c,
								Name, Allocation_Status__c, sked__Job_Status__c, sked__Description__c, sked__Notes_Comments__c,
								sked__Duration__c, sked__Region__c, Opportunity__c, sked__Type__c, sked__Parent__c, 
								sked__Address__c, sked_Total_Required_Resources__c,  
								 sked__Contact__c, sked__Contact__r.Name, sked__GeoLocation__Latitude__s,
								sked__GeoLocation__Longitude__s
								FROM sked__Job__c 
								WHERE id = : jobId
								];

		skedModels.job job = new skedModels.job(skedJob);
		skedModels.jobTask jt = new skedModels.jobTask();
		jt.sequence = 3;
		jt.taskname = 'DCP Details Completed';
		job.jobTasks.add(jt);

		skedModels.requiredResource rr = new skedModels.requiredResource();
		rr.category = 'Electrician';
		rr.isChanged = true;
		rr.numOfResources = 1;

		job.requiredResources.add(rr);
		skedRemoteResultModel result = skedManageServiceDeliveryController.saveJob(JSON.serialize(job));
		System.debug('result = ' + result);
		test.stopTest();
	}

	static testmethod void saveJobTestMissingRegion() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		test.startTest();
		Id jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Timezone__c, sked__Finish__c, sked_Job_Completion_Status__c,
								Name, Allocation_Status__c, sked__Job_Status__c, sked__Description__c, sked__Notes_Comments__c,
								sked__Duration__c, sked__Region__c, Opportunity__c, sked__Type__c, sked__Parent__c, 
								sked__Address__c, sked_Total_Required_Resources__c,  
								 sked__Contact__c, sked__Contact__r.Name, sked__GeoLocation__Latitude__s,
								sked__GeoLocation__Longitude__s
								FROM sked__Job__c 
								WHERE id = : jobId
								];

		//test missing region
		skedJob.sked__region__c = null;		
		skedModels.job job = new skedModels.job(skedJob);
		skedManageServiceDeliveryController.saveJob(JSON.serialize(job));

		test.stopTest();
	}

	static testmethod void saveJobTestJobCancelled() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		test.startTest();
		Id jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Timezone__c, sked__Finish__c, sked_Job_Completion_Status__c,
								Name, Allocation_Status__c, sked__Job_Status__c, sked__Description__c, sked__Notes_Comments__c,
								sked__Duration__c, sked__Region__c, Opportunity__c, sked__Type__c, sked__Parent__c, 
								sked__Address__c, sked_Total_Required_Resources__c,  
								 sked__Contact__c, sked__Contact__r.Name, sked__GeoLocation__Latitude__s,
								sked__GeoLocation__Longitude__s
								FROM sked__Job__c 
								WHERE id = : jobId
								];

		//test missing region
		skedJob.sked__Job_Status__c = SkeduloConstants.JOB_STATUS_CANCELLED;	
		skedModels.job job = new skedModels.job(skedJob);
		skedManageServiceDeliveryController.saveJob(JSON.serialize(job));

		test.stopTest();
	}

	static testmethod void saveJobTestInsertJob() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		test.startTest();
		Id jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		sked__Job__c skedJob = [SELECT Id, sked__Start__c, sked__Timezone__c, sked__Finish__c, sked_Job_Completion_Status__c,
								Name, Allocation_Status__c, sked__Job_Status__c, sked__Description__c, sked__Notes_Comments__c,
								sked__Duration__c, sked__Region__c, Opportunity__c, sked__Type__c, sked__Parent__c, 
								sked__Address__c, sked_Total_Required_Resources__c,  
								 sked__Contact__c, sked__Contact__r.Name, sked__GeoLocation__Latitude__s,
								sked__GeoLocation__Longitude__s
								FROM sked__Job__c 
								WHERE id = : jobId
								];

		//test missing region
		skedJob.id = null;	
		skedModels.job job = new skedModels.job(skedJob);
		skedManageServiceDeliveryController.saveJob(JSON.serialize(job));

		test.stopTest();
	}

	static testmethod void saveAssignedResourcesTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		sked__Job__c skedJob = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
		String startDateString = skedJob.sked__Start__c.format('yyyy-MM-dd');
		Integer startTime = 1000;
		Integer durationInMinutes = 60;
		sked__Resource__c skedRes = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
		sked__Resource_Tag__c skedResTag = [SELECT id, sked__Tag__c FROM sked__Resource_Tag__c WHERE sked__Resource__c = :skedRes.id LIMIT 1];
		List<sked_Resource_Requirement_Defaults__c> rrdList = (list<sked_Resource_Requirement_Defaults__c>)objFactory.getSObjects(sked_Resource_Requirement_Defaults__c.sObjectType);
		skedManageServiceDeliveryController.resourceDefault rd = new skedManageServiceDeliveryController.resourceDefault('Test job Type', rrdList);
		skedModels.resource res = new skedModels.resource();
		res.assigned = true;
		res.category =  skedRes.sked__Category__c;
		res.categoryType = 'Person';
		res.distance = 100;
		res.id = skedRes.id;
		res.isAvailable = true;
		res.isLocked = false;
		res.isTeamLead = true;
		res.mobilePhone = '0905012014';
		res.name = skedRes.name;
		res.regionId = skedRes.sked__Primary_Region__c;
		res.regionName = 'Queensland';
		res.startTime = 800;
		res.tags = new List<String>{skedResTag.sked__Tag__c};
		res.travelTimeFrom = 12;
		res.requiredResourceId = rrdList.get(0).id;
		//res.isDisabled = false;
		test.startTest();
		skedManageServiceDeliveryController.saveAssignedResources(skedJob.id, JSON.serialize(new List<skedModels.resource>{res}),
																	startDateString,startTime,durationInMinutes);
		test.stopTest();
	}

	static testmethod void saveAssignedResourcesTest2() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		sked__Job__c skedJob = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
		String startDateString = skedJob.sked__Start__c.format('yyyy-MM-dd');
		Integer startTime = 1000;
		Integer durationInMinutes = 60;
		sked__Resource__c skedRes = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
		sked__Resource_Tag__c skedResTag = [SELECT id, sked__Tag__c FROM sked__Resource_Tag__c WHERE sked__Resource__c = :skedRes.id LIMIT 1];
		List<sked_Resource_Requirement_Defaults__c> rrdList = (list<sked_Resource_Requirement_Defaults__c>)objFactory.getSObjects(sked_Resource_Requirement_Defaults__c.sObjectType);
		skedManageServiceDeliveryController.resourceDefault rd = new skedManageServiceDeliveryController.resourceDefault('Test job Type', rrdList);
		skedModels.resource res = new skedModels.resource();
		res.assigned = true;
		res.category =  skedRes.sked__Category__c;
		res.categoryType = 'Person';
		res.distance = 100;
		res.id = skedRes.id;
		res.isAvailable = true;
		res.isLocked = false;
		res.isTeamLead = true;
		res.mobilePhone = '0905012014';
		res.name = skedRes.name;
		res.regionId = skedRes.sked__Primary_Region__c;
		res.regionName = 'Queensland';
		res.startTime = 800;
		res.tags = new List<String>{skedResTag.sked__Tag__c};
		res.travelTimeFrom = 12;
		res.requiredResourceId = rrdList.get(0).id;
		//res.isDisabled = false;
		List<sked__Job_Allocation__c> skedJAs = [SELECT id FROM sked__Job_Allocation__c WHERE sked__Job__c = :skedJob.id];
		delete skedJAs;
		skedJob.sked__Job_Status__c = SkeduloConstants.JOB_STATUS_PENDING_ALLOCATION;
		update skedJob;
		test.startTest();

		skedManageServiceDeliveryController.saveAssignedResources(skedJob.id, JSON.serialize(new List<skedModels.resource>{res}),
																	startDateString,startTime,durationInMinutes);
		test.stopTest();
	}

	static testmethod void saveServiceLocationJobTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		Contact contact = (Contact)objFactory.getSObjects(Contact.sObjectType).get(0);
		Opportunity opp = (Opportunity)objFactory.getSObjects(Opportunity.sObjectType).get(0);
		sked__Region__c region 	= (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);

		skedModels.ServiceLocationJobModel job = new skedModels.ServiceLocationJobModel();
		job.address = '5 Burnett Ln, Brisbane City QLD 4000, Australia';
		job.contact = contact.id;
		job.description = 'ancbsdfjsf';
		job.durationInMinutes = 120;
		job.startTime = DateTime.newInstance(System.today(), Time.newInstance(8, 0, 0, 0)).getTime();
		job.endTime = DateTime.newInstance(System.today(), Time.newInstance(10, 0, 0, 0)).getTime();
		job.ignorePublicHolidays = true;
		job.isParent = true;
		job.isReplicated = false;
		job.jobTasks = new List<skedModels.jobTask>();
		job.jobType = 'Commercial site';
		job.latitude  = -27.4705306;
		job.longitude = 153.0233795;
		job.multipleDays = false;
		job.opportunityId = opp.id;
		job.regionId = region.id;
		job.requiredResources = new List<skedModels.requiredResource>();

		skedModels.jobTask jt = new skedModels.jobTask();
		jt.sequence = 3;
		jt.taskname = 'DCP Details Completed';
		job.jobTasks.add(jt);

		skedModels.requiredResource rr = new skedModels.requiredResource();
		rr.category = 'Electrician';
		rr.isChanged = true;
		rr.numOfResources = 1;

		job.requiredResources.add(rr);

		test.startTest();
		skedRemoteResultModel result = skedManageServiceDeliveryController.saveServiceLocationJob(job);
		System.debug('result ' + result);
		test.stopTest();
	}

	static testmethod void saveServiceLocationJobTestMissingRegion() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		Contact contact = (Contact)objFactory.getSObjects(Contact.sObjectType).get(0);
		Opportunity opp = (Opportunity)objFactory.getSObjects(Opportunity.sObjectType).get(0);
		sked__Region__c region 	= (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);

		skedModels.ServiceLocationJobModel job = new skedModels.ServiceLocationJobModel();
		job.address = '5 Burnett Ln, Brisbane City QLD 4000, Australia';
		job.contact = contact.id;
		job.description = 'ancbsdfjsf';
		job.duration = '1.00h';
		job.startTime = 800;
		job.ignorePublicHolidays = true;
		job.isParent = true;
		job.isReplicated = false;
		job.jobTasks = new List<skedModels.jobTask>();
		job.jobType = 'Commercial site';
		job.latitude  = -27.4705306;
		job.longitude = 153.0233795;
		job.multipleDays = false;
		job.opportunityId = opp.id;
		job.regionId = '';
		job.requiredResources = new List<skedModels.requiredResource>();

		skedModels.jobTask jt = new skedModels.jobTask();
		jt.sequence = 3;
		jt.taskname = 'DCP Details Completed';
		job.jobTasks.add(jt);

		skedModels.requiredResource rr = new skedModels.requiredResource();
		rr.category = 'Electrician';
		rr.isChanged = true;
		rr.numOfResources = 1;

		job.requiredResources.add(rr);

		test.startTest();
		skedManageServiceDeliveryController.saveServiceLocationJob(job);
		test.stopTest();
	}

	static testmethod void getDefaultResourceForJobTypeTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

		Job_Type__c skedJobType = (Job_Type__c)objFactory.getSObjects(Job_Type__c.sObjectType).get(0);
		test.startTest();
		skedManageServiceDeliveryController.getDefaultResourceForJobType(skedJobType.name);
		skedCalloutException ex = new skedCalloutException(400, 'FALSE', 'abc');
		skedCalloutResultBase ab = new skedCalloutResultBase();
		ab.message = 'abc';
		ab.response = 'abc';
		test.stopTest();
	}

	static testmethod void createJobTimeConstraintTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        
		Id jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		List<Id> jobIds = new List<id>{jobId};

		test.startTest(); 
		skedJobTimeConstraintHandler.createJobTimeConstraint(jobIds);
		test.stopTest();
	}

	static testmethod void getOppIdTest() {
		skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
		String jobId = ((sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0)).id;
		
		test.startTest(); 
		skedLightningAllocateModalDataCtrl.getOppId(jobId);
		test.stopTest();
	}
	
}