@isTest
private class skedSObjectFactory_Test {
	@isTest
	private static void test_initSampleTestData(){
		skedSObjectFactory objFactory = new skedSObjectFactory();
		objFactory.init().create();
        
        printData();
    }
    
    public static void printData(){
        list<Account> accounts = [Select Id, Name from Account];
		system.debug('accounts: ' + accounts.size());
		for(Account acc : accounts){
			System.debug('Account: ' + acc.Name );
		}

		//sked__Region__c region = [Select Id from sked__Region__c where sked__Timezone__c = 'Australia/Queensland'];
		list<sked__Region__c> regions = [Select Id, Name, sked__Timezone__c, sked__Country_Code__c from sked__Region__c];
		///System.assertEquals(3, regions.size());
		for(sked__Region__c region : regions){
			System.debug('Region: ' + region.Name + ' - ' + region.sked__Country_Code__c);
		}

		list<Contact> contacts = [Select Id, Name, sked__Region__r.sked__Timezone__c from Contact];
		system.debug('contacts: ' + contacts.size());
		for(Contact contact : contacts){
			System.debug('contact: ' + contact.Name + ' - ' + contact.sked__Region__r.sked__Timezone__c);
		}

		list<sked__Job__c> jobs = [Select Id, Name, sked__Start__c, sked__Job_Status__c, sked__Duration__c, sked__Region__r.sked__Timezone__c from sked__Job__c];
		system.debug('Jobs: ' + jobs.size());
		for(sked__Job__c job : jobs){
			System.debug('Job: ' + Job.Name + ' - ' + job.sked__Job_Status__c + ' - ' + job.sked__Region__r.sked__Timezone__c);
		}

		list<sked__Resource__c> resources = [Select Id, Name, sked__Resource_Type__c, sked__Primary_Region__r.Name from sked__Resource__c];
		system.debug('resources: ' + resources.size());
		for(sked__Resource__c r : resources){
			System.debug('Resource: ' + r.Name + ' - ' + r.sked__Resource_Type__c + ' - ' + r.sked__Primary_Region__r.Name);
		}

		list<sked__Tag__c> tags = [Select Id, Name from sked__Tag__c];
		system.debug('tags: ' + tags.size());
		for(sked__Tag__c t : tags){
			System.debug('Tag: ' + t.Name);
		}

		list<sked__Resource_Tag__c> rtList = [Select Id, Name from sked__Resource_Tag__c];
		system.debug('Resource Tags: ' + rtList.size());

		list<sked__Job_Allocation__c> jaList = [Select Id, Name from sked__Job_Allocation__c];
		system.debug('Job Allocation: ' + jaList.size());

		list<sked__Availability_Template_Entry__c> teList = [Select Id, Name, sked__Weekday__c from sked__Availability_Template_Entry__c];
		system.debug('teList: ' + teList.size());
		for(sked__Availability_Template_Entry__c te : teList){
			System.debug('Template Entry: ' + te.sked__Weekday__c);
		}

		list<sked__Availability_Template_Resource__c> atlList = [Select Id, Name from sked__Availability_Template_Resource__c];
		system.debug('Availability Template Resource: ' + atlList.size());
        
        list<sked__Availability__c> aList = [Select Id, Name, sked__Resource__c from sked__Availability__c];
		system.debug('Availability: ' + aList.get(0).Name + ' resource: ' + aList.get(0).sked__Resource__c);
    }
}