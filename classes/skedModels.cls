global class skedModels {
	global virtual class geometry {
        public decimal lat {get; set;}
        public decimal lng {get; set;}

        public geometry(Location geoLocation) {
            if (geoLocation != null) {
                this.lat = geoLocation.getLatitude();
                this.lng = geoLocation.getLongitude();
            }
        }

        public geometry(decimal lat, decimal lng) {
            this.lat = lat;
            this.lng = lng;
        }
    }

    /* 

        Opportunity Model 

    */
    global class skedOpportunity{
        public string id {get;set;}
        public string oppType {get;set;}
        public string name {get;set;}
        public string accId {get;set;}
        public string opportunityOwner {get;set;}
        public string address {get;set;}
        public integer requiredNoOfRigs {get;set;}
        public List<job> jobDetails {get;set;}
        public skedOpportunity(){
            this.jobDetails = new list<job>();
        }
        public skedOpportunity(Opportunity opp){
            this.id = opp.id;
            this.name = opp.Name;
            this.accId = opp.accountId;
            this.opportunityOwner = opp.Owner.Name;
            
            this.jobDetails = new list<job>();
            this.oppType = opp.Type;
        }
    }

	global class job {
        public string id {get;set;}
        public string startDateString {get;set;}
        public integer startTime {get;set;}
        public String formatedStartTime {get;set;}
        public String formatedEndTime {get;set;}
        public string jobNumber {get;set;}
        public string groupAllocationStatus {get;set;}
        public string allocationStatus {get;set;}
        public string jobStatus {get;set;}
        public string description {get;set;}
        public string comment {get;set;}
        public integer durationInMinutes {get;set;}
        public string duration {get;set;}
        public string regionId {get;set;}
        public string address {get;set;}
        public string contact {get;set;}
        public string accountId;
        public string contactName {get;set;}
        public string opportunityId {get;set;}
        public string jobCompletionStatus {get;set;}
        public string jobType {get;set;}
        public string jobCategory {get;set;}
        public boolean isParent {get;set;}
        public string parentId {get;set;}
        public boolean multipleDays {get;set;}
        public boolean ignorePublicHolidays {get;set;}
        public boolean timeTravelToBePaid {get;set;}
        public list<jobTask> jobTasks {get;set;}
        public List<string> weekdayTemplate {get;set;}
        public List<string> selectedDates {get;set;}
        public List<resource> resources {get;set;} 
        //public List<address> addresses {get;set;}
        public List<requiredResource> requiredResources {get;set;}
        
        public List<job> childJobs {get;set;}

        public List<String> deletedProducts {get;set;}
        //public List<string> deletedAddresses {get;set;}
        public List<string> deletedCategories {get;set;}
        public List<string> deletedJobTasks {get;set;}
        public string timezoneSidId {get;set;}
        public boolean isEditable {get;set;}
        public boolean isDeleted {get;set;}
        public boolean isChanged {get;set;}
        
        public integer totalRequiredResources {get;set;}
        public integer totalRequiredTeamLeader {get;set;}
        public integer totalRequiredOtherResources {get;set;}
        public integer totalRequiredAssets {get;set;}
        public integer allocatedTeamLeader {get;set;}
        public integer allocatedResources {get;set;}
        public integer allocatedAssets {get;set;}

        public Decimal latitude {get;set;}
        public Decimal longitude {get;set;}
        
        public transient DateTime jobStart {get;set;}
        public transient DateTime jobFinish {get;set;}
        
        public job(){}
        
        public job(sked__Job__c skedJob) {
            this.id = skedJob.Id;
            if (skedJob.sked__Start__c != NULL) {
                this.startDateString = skedJob.sked__Start__c.format('yyyy-MM-dd', skedJob.sked__Timezone__c);
                this.startTime = integer.valueOf(skedJob.sked__Start__c.format('HHmm', skedJob.sked__Timezone__c));
                this.formatedStartTime = skedJob.sked__Start__c.format('hh:mm a', skedJob.sked__Timezone__c);
                this.formatedEndTime = skedJob.sked__Finish__c.format('hh:mm a', skedJob.sked__Timezone__c);
            }
            this.jobCompletionStatus = skedJob.sked_Job_Completion_Status__c;
            this.jobNumber = skedJob.Name;
            this.allocationStatus = skedJob.Allocation_Status__c;
            this.jobStatus = skedJob.sked__Job_Status__c;
            this.description = skedJob.sked__Description__c;
            this.comment = skedJob.sked__Notes_Comments__c;
            this.durationInMinutes = integer.valueOf(skedJob.sked__Duration__c);
            this.regionId = skedJob.sked__Region__c;
            this.opportunityId = skedJob.Opportunity__c;
            this.jobType = skedJob.sked__Type__c;
            this.parentId = skedJob.sked__Parent__c;
            
            this.timezoneSidId = skedJob.sked__Timezone__c;
            this.isEditable = true;
            this.address = skedJob.sked__Address__c;
            
            this.totalRequiredResources = integer.valueOf(skedJob.sked_Total_Required_Resources__c);

            
            this.resources = new List<resource>();
            this.requiredResources = new List<requiredResource>();
            this.childJobs = new List<job>();
            this.jobTasks = new list<jobTask>();
            this.contact = skedJob.sked__Contact__c;
            this.contactName = skedJob.sked__Contact__r.Name;
            this.latitude = skedJob.sked__GeoLocation__Latitude__s;
            this.longitude = skedJob.sked__GeoLocation__Longitude__s;
            
            for (sked__Job_Allocation__c jobAlloc : skedJob.sked__Job_Allocations__r) {
                resource resource = new resource(jobAlloc);
                this.resources.add(resource);
            }

            for (sked__Job_Task__c jt: skedJob.sked__Job_Tasks__r){
                jobTask jtask = new jobTask(jt);
                this.jobTasks.add(jtask);
            }
        }        
    }

    global class ServiceLocationJobModel {
        public String jobId {get;set;}
        public String regionId {get;set;}
        public String address {get;set;}
        public String associatedJob {get;set;}
        public String contact {get;set;}
        public String description {get;set;}
        public String opportunityId {get;set;}
        public Long startTime {get;set;}
        public Long endTime {get;set;}
        public String duration {get;set;}
        public Integer durationInMinutes {get;set;}
        public String jobType {get;set;}
        public list<jobTask> jobTasks {get;set;}
        public List<requiredResource> requiredResources {get;set;}        
        public boolean isParent {get;set;}
        public String parentId {get;set;}    
        public String jobStatus {get;set;}  
        public List<string> selectedDates {get;set;}
        public boolean ignorePublicHolidays {get;set;}
        public boolean isReplicated {get;set;}
        public boolean multipleDays {get;set;}
        public Decimal longitude {get;set;}
        public Decimal latitude {get;set;}
    }
    
    global class allocation {
        public string id {get;set;}
        public string requiredResourceId {get;set;}
        public string jobId {get;set;}
        public string jobDate {get;set;}
        public string jobName {get;set;}
        public string resourceId {get;set;}
        public string resourceName {get;set;}
        public string conflictType {get;set;}
        public string conflictDetails {get;set;}
        public string mobilePhone {get;set;}
        public transient boolean isValid {get;set;}
        public boolean isAvailable {get;set;}   
        public boolean bypassCheck {get;set;}     
        public decimal distance {get;set;}
        public integer travelTimeFrom {get;set;}
        public integer travelTimeTo {get;set;}
        public dateTime expectedStartTime {get;set;}
        public string accountId {get; set;}

        public transient resource resource {get;set;}
        public transient dateTime startTime {get;set;}
        public transient dateTime endTime {get;set;}
        public transient geometry geoLocation {get;set;}
        public eventModel previousEvent;
        public transient eventModel nextEvent;
        public transient decimal maxTravelTime {get; set;}        
    }

    global virtual class journey {
        public geometry origin {get; set;}
        public geometry destination {get; set;}
        public integer travelTime {get; set;}
        public decimal distance {get; set;}
        public string status {get;set;}
    }
    
    global virtual class resource implements Comparable {
        public string id {get;set;}
        public string name {get;set;}
        public boolean isTeamLead {get;set;}
        public string category {get;set;}
        public string categoryType {get;set;}
        public string photoUrl {get;set;}
        public boolean assigned {get;set;}
        public boolean isLocked {get;set;}
        public boolean bypassCheck {get;set;}
        public string regionId {get;set;}
        public string regionName {get;set;}
        public decimal distance {get;set;}
        public transient string travelMethod {get;set;}
        public decimal maxTravelDistance {get;set;}
        public boolean isAvailable {get;set;}
        public string requiredResourceId {get;set;}
        public integer startTime {get;set;}
        public integer travelTimeFrom {get;set;}
        public integer travelTimeTo {get;set;}
        public List<String> tags {get;set;}
        public string mobilePhone {get;set;}
        public string unavailableType {get;set;}
        public string jaID {get; set;}
        public string jaStatus {get; set;}

        public resource() { 
            this.tags = new List<String>();
            this.isLocked = false;
        }
        
        public resource(sked__Job_Allocation__c jobAlloc) {
            this.id = jobAlloc.sked__Resource__c;
            this.name = jobAlloc.sked__Resource__r.Name;
            this.category = jobAlloc.sked__Resource__r.sked__Category__c;
            this.photoUrl = jobAlloc.sked__Resource__r.sked__User__r.SmallPhotoUrl;
            this.regionId = jobAlloc.sked__Resource__r.sked__Primary_Region__c;
            this.regionName = jobAlloc.sked__Resource__r.sked__Primary_Region__r.Name;
            this.assigned = true;
            this.isLocked = false;
            this.jaID = jobAlloc.id;
            this.jaStatus = jobAlloc.sked__Status__c != null ? jobAlloc.sked__Status__c : '';
            this.isTeamLead = jobAlloc.sked__Team_Leader__c;
            this.travelMethod = jobAlloc.sked__Resource__r.Travel_Method__c;
            this.mobilePhone = jobAlloc.sked__Resource__r.sked__Mobile_Phone__c;
            populateStartTime(jobAlloc);
        }

        public void populateJAInfo(sked__Job_Allocation__c ja) {
            this.jaID = ja.id;
            this.jaStatus = ja.sked__Status__c != null ? ja.sked__Status__c : '';
            this.isTeamLead = ja.sked__Team_Leader__c;
        }
        
        public void populateStartTime(sked__Job_Allocation__c jobAlloc) {
            DateTime expectedStartTime = jobAlloc.Expected_Start_Time__c == NULL ? jobAlloc.sked__Job__r.sked__Start__c : jobAlloc.Expected_Start_Time__c;
            this.startTime = integer.valueOf(expectedStartTime.format('Hmm', jobAlloc.sked__Job__r.sked__Timezone__c));
        }
        
        public Integer compareTo(Object compareTo) {
            resource compareToRecord = (resource)compareTo;
            Integer returnValue = 0;
            
            if (distance == -1) {
                returnValue = 1;
            } else if (compareToRecord.distance == -1) {
                returnValue = -1;
            } else if (distance > compareToRecord.distance) {
                returnValue = 1;
            } else if (distance < compareToRecord.distance) {
                returnValue = -1;
            }
            return returnValue;   
        }
    }
    
    global class requiredResource {
        public string id {get;set;}
        public string category {get;set;}
        public string categoryType {get;set;}
        public string jobId {get;set;}
        public integer numOfResources {get;set;}
        public List<string> tags {get;set;}
        public List<resource> availResources {get;set;}
        public boolean isChanged {get;set;}
        
        
        public transient integer numOfAssignedResources {get;set;}
        public transient string tempId {get;set;}
        public transient Set<Id> availResourceIds {get;set;}
        
        public requiredResource() {
            this.tags = new List<String>();
            this.availResources = new List<resource>();
        }

        public requiredResource(sked__Resource_Requirement__c req) {
            this.id = req.id;
            this.category = req.sked_Category__c;
            this.categoryType = req.sked_Category_Type__c;
            this.jobId = req.sked__Job__c;
            this.numOfResources = (Integer)req.sked__Quantity__c;
            this.tags = new List<String>();

            if (req.sked__Resource_Requirement_Tags__r != null) {
                for (sked__Resource_Requirement_Tag__c reqTag : req.sked__Resource_Requirement_Tags__r) {
                    this.tags.add(reqTag.sked__Tag__c);
                }
            }

            this.isChanged = false;
            this.availResources = new List<resource>();
            this.availResourceIds = new Set<Id>();
        }
    }


    //JobTags
    global class jobTask{
        public string taskName {get;set;}
        public string description {get;set;}
        public integer sequence {get;set;}
        //public string address{get;set;}
        //public boolean isPrimary {get;set;}
        public string jobId {get;set;}
        public string id {get;set;}
        public boolean isChanged {get;set;}
        public jobTask(){}

        public jobTask(sked__Job_Task__c jobTask){
            this.taskName = jobTask.Name;
            this.description = jobTask.sked__Description__c;
            this.sequence = (integer)jobTask.sked__Seq__c;
            this.jobID = jobTask.sked__Job__c;
            this.id = jobTask.id;

        }
    }
    
    global virtual class selectOption {
        public object id {get;set;}
        public string label {get;set;}
        
        public selectOption(object value, string text) {
            this.id = value;
            this.label = text;
        }
    }
    
    global class dayslotList {
        public string regionId {get;set;}
        public string jobType {get;set;}
        public string timezone {get;set;}
        public List<dayslot> dayslots {get;set;}
        public transient Map<DateTime, timeslot> mapTimeslot {get;set;}
        
        public dayslotList() {
            this.dayslots = new List<dayslot>();
            this.mapTimeslot = new Map<DateTime, timeslot>();
        }
    }
    
    global class dayslot {
        public string slotDate {get;set;}
        public List<timeSlot> timeSlots {get;set;}
        
        public dayslot() {
            this.timeSlots = new List<timeSlot>();
        }
    }
    
    global class timeSlot implements Comparable {
        public string regionId {get;set;}
        public string slotDate {get;set;}
        public string startTime {get;set;}
        public string endTime {get;set;}
        public integer totalResources {get;set;}
        public integer noOfAvailResources {get;set;}
        public List<requiredResource> requiredResources {get;set;}
        public string dayOfWeek {get;set;}
        public List<resource> availableResources {get;set;}
        
        public transient string dateString {get;set;}
        public DateTime startTimeValue {get;set;}
        public DateTime endTimeValue {get;set;}
        public transient Map<Id, requiredResource> mapRequiredResources {get;set;}
        public transient Map<Id, resource> mpAvailableResources {get;set;}

        public timeSlot() {
            this.totalResources = 0;
            this.noOfAvailResources = 0;
            this.requiredResources = new List<requiredResource>();
            this.mapRequiredResources = new Map<Id, requiredResource>();
            this.mpAvailableResources = new Map<Id, resource>();
            this.availableResources = new List<resource>();
        }
        
        public Integer compareTo(Object compareTo) {
            timeSlot compareToRecord = (timeSlot)compareTo;
            Integer returnValue = 0;
            
            if (startTimeValue > compareToRecord.startTimeValue) {
                returnValue = 1;
            } else if (startTimeValue < compareToRecord.startTimeValue) {
                returnValue = -1;
            }
            return returnValue;   
        }
    }      

    global class ResourceFilter {
        public List<requiredResource> requiredResources;
        public decimal maxTravelTime;
        public string contactId;
        public string accountId;
        
        public ResourceFilter() {
            this.requiredResources = new List<requiredResource>();
        }
    }
    
    global class AssociatedJob {
        public String jobId;
        public String opportunityName;
        public String jobDate;
        public String jobStartTime;        
        
        public AssociatedJob () {}

        public AssociatedJob(sked__Job__c job) {
            this.jobId = job.Id;
            this.opportunityName = job.Opportunity__r.Name;            
            this.jobDate = job.sked__Start__c.format('dd/MM/yyyy');
            this.jobStartTime = job.sked__Start__c.format('HH:mm');
        }
    }
    
    global class AssociatedContact {
        public String contactId;
        public String contactName;
        public String email;
        public String accountId;

        public AssociatedContact(Contact c) {
            this.contactId = c.Id;
            this.contactName = c.Name;
            this.email = c.Email;
            this.accountId = c.AccountId;
        }
    }    

    global class eventModel implements Comparable {
        public DateTime start {get;set;}
        public DateTime finish {get;set;}
        public string eventType {get;set;}
        public Id relatedId {get;set;}
        public Location geoLocation {get;set;}
        public string eventDetails {get;set;}
        public string jobType;
                
        public Integer compareTo(Object compareTo) {
            eventModel compareToRecord = (eventModel)compareTo;
            Integer returnValue = 0;
            
            if (start > compareToRecord.start) {
                returnValue = 1;
            } else if (start < compareToRecord.start) {
                returnValue = -1;
            } else {
                if (finish > compareToRecord.finish) {
                    returnValue = -1;
                } else if (finish < compareToRecord.finish) {
                    returnValue = 1;
                }
            }
            return returnValue;   
        }
    }
}