global class skedSetting {
	private skedAdminSetting adminSetting;

	public skedAdminSetting Admin {
        get {
            if (adminSetting == null) {
                adminSetting = new skedAdminSetting();
            }
            return adminSetting;
        }
    }

    /*********************************************************** Singleton stuffs ***********************************************************/
    private static skedSetting mInstance = null;

    public static skedSetting instance {
        get {
            if (mInstance == null) {
                mInstance = new skedSetting();
            }
            return mInstance;
        }
    }

    /*********************************************************** Private constructor ***********************************************************/
    private skedSetting() {}

    /*********************************************************** Nested Classes ***********************************************************/
    public class skedAdminSetting {
        public integer velocity {get;set;}
        public string distanceMeasurementUnit {get;set;}
        public string googleAPI {get; set;}
        public string skeduloAPIToken {get;set;}
        
        public skedAdminSetting() {
            sked_Admin_Settings__c setting = sked_Admin_Settings__c.getOrgDefaults();
        	this.velocity = 30;
            this.distanceMeasurementUnit = setting.sked_Distance_Measurement_Unit__c;
            this.googleAPI = setting.Google_API_Key__c;
            this.skeduloAPIToken = setting.Skedulo_API_Token__c;
		}
    }
}