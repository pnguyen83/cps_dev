global class skedMapper {

    public static sked__Job_Task__c toObject(skedModels.JobTask model){
        sked__Job_Task__c jobTask = new sked__Job_Task__c(
            Name = model.taskName,

            sked__Description__c = model.description,
          
            sked__Job__c = String.isBlank(model.jobID)?null:model.jobID,
            sked__Seq__c = model.sequence
            );

        return jobTask;
    }

    public static sked__Resource_Requirement__c toObject(skedModels.requiredResource model) {
        sked__Resource_Requirement__c jobResourceInfo = new sked__Resource_Requirement__c(
            sked__Job__c = model.jobId,
            sked_Category__c = model.category,
            sked_Category_Type__c = model.categoryType,
            sked__Quantity__c = model.numOfResources
        );
        if (!string.isBlank(model.id)) {
            jobResourceInfo.Id = model.id;
        }
        return jobResourceInfo;
    }
    
    public static sked__Job__c toObject(skedModels.job model) {
        System.debug('model ' + model);
        Date jobDate = (Date)Json.deserialize(model.startDateString, Date.class);
        Integer jobHour = integer.valueOf(model.startTime / 100);
        Integer jobMinute = Math.mod(model.startTime, 100);
        
        DateTime jobDateStart = DateTime.newInstance(jobDate, Time.newInstance(0, 0, 0, 0));
        jobDateStart = skedUtils.ConvertBetweenTimezones(jobDateStart, model.timezoneSidId, UserInfo.getTimezone().getID());
        model.jobStart = jobDateStart.addHours(jobHour);
        model.jobStart = model.jobStart.addMinutes(jobMinute);
        //model.isParent = true;
        /*
        if (string.isBlank(model.id)) {
            if (model.duration.contains('h')) {
                Decimal hours = Decimal.valueOf(model.duration.replace('h', ''));
                model.durationInMinutes = (Integer)(hours * 60);                
            }
            else {
                integer days = integer.valueOf(model.duration.replace('d', ''));
                model.durationInMinutes = 8 * 60;                
            }
        }
        else {
            if ( model.duration.contains('h') ) {
                Decimal JobDuration = ((Decimal)model.durationInMinutes)/60;                
                model.duration = String.valueOf(JobDuration) + 'h';
            }
        }*/

        model.jobFinish = model.jobStart.addMinutes(model.durationInMinutes);

        sked__Job__c obj = new sked__Job__c(
            sked__Notes_Comments__c = model.comment,
            sked__Duration__c = model.durationInMinutes,
            sked__Description__c = model.description,
            sked__Region__c = model.regionId,
            sked__Address__c = model.address,
            sked__Start__c = model.jobStart,
            sked__Finish__c = model.jobFinish,
            sked__Type__c = model.jobType,
            Opportunity__c = model.opportunityId,
            sked__Job_Status__c = model.jobStatus,
            sked__Parent__c = model.parentId == NULL ? NULL : model.parentId,
            sked__GeoLocation__Latitude__s = model.latitude,
            sked__GeoLocation__Longitude__s = model.longitude
        );
        if (!string.isBlank(model.id)) {
            obj.Id = model.id;
        }
        

        return obj;
    }

    public static sked__Job__c toObject(skedModels.ServiceLocationJobModel jobModel) {
        Long startTime = DateTime.newInstance(jobModel.startTime).getTime();
        Long endTime = DateTime.newInstance(jobModel.endTime).getTime();
        Long durationInMinutes = (endTime - startTime)/(60*1000);
        sked__Job__c obj = new sked__Job__c(            
            sked__Duration__c = durationInMinutes,
            sked__Description__c = jobModel.description,
            sked__Region__c = jobModel.regionId,
            sked__Address__c = jobModel.address,
            sked__Start__c = DateTime.newInstance(jobModel.startTime),
            sked__Finish__c = DateTime.newInstance(jobModel.endTime),
            sked__Type__c = jobModel.jobType,            
            Opportunity__c = jobModel.opportunityId,
            sked__Job_Status__c = jobModel.jobStatus,
            sked__GeoLocation__Latitude__s = jobModel.latitude,
            sked__GeoLocation__Longitude__s = jobModel.longitude
        );


        if ( jobModel.isParent == FALSE && jobModel.parentId != NULL ) {
            obj.sked__Parent__c = jobModel.parentId;
        }
        if (!string.isBlank(jobModel.jobId)) {
            obj.Id = jobModel.jobId;
        }

        return obj;
    }
    
    public static sked__Job_Allocation__c toObject(skedModels.allocation model) {
        sked__Job_Allocation__c allocation = new sked__Job_Allocation__c(
        	sked__Job__c = model.jobId,
            sked__Resource__c = model.resourceId,
            sked__Resource_Requirement__c = model.requiredResourceId,
            Expected_Start_Time__c = model.expectedStartTime
        );
        if (!string.isBlank(model.id)) {
            allocation.Id = model.id;
        }
        return allocation;
    }
    
}