global class skedSkeduloApiData {
    
    public static final string LOCATION_HISTORY_EVENT_TYPE_ENROUTE = 'ENROUTE';
    public static final string LOCATION_HISTORY_EVENT_TYPE_CHECKEDIN = 'CHECKEDIN';
    
    global virtual class resultBase extends skedCalloutResultBase {}
    
    /* DISTANCE MATRIX */
    public class distanceMatrixResult extends resultBase {
        public Matrix data {get;set;}
        
        public distanceMatrixResult() {
            super();
        }
    }
    
    public class Matrix {
        public List<List<Element>> matrix {get;set;}
    }
    
    public class Distance {
        public integer distanceInMeters {get;set;}
    }
    
    public class Duration {
        public integer durationInSeconds {get;set;}
    }
    
    public class Element {
        public Distance distance {get;set;}
        public Duration duration {get;set;}
        public string status {get;set;}
    }

}